# **ENGG200/300/600 - L3_C_10**

Repository for main code-base's for team **L3_C_10** in ENGG200/300/600. This code-base will be used for both lift 3 & lift 9.


## **Directory structure**

Explanations for directories and their containing files are detailed below:

```
.
├── .vscode               # VSCode config files
│   ├── arduino.json      # Config file for the Arduino extension in VSCode
│   ├── c_cpp_properties  # Config file for the C/C++ extension in VSCode
│   └── launch.json       # Config file for running '.ino' files in VSCode
│
├── Lift 3                # Lift 3 code-base
│   ├── Comms.ino         # Comms code for Arduino MEGA (for testing)
│   └── ControlBox.ino    # Control Box code for Arduino UNO (for production)
│
├── Lift 9                # Lift 9 code-base
│   ├── Comms.ino         # Comms code for Arduino MEGA (for testing)
│   └── ControlBox.ino    # Control Box code for Arduino UNO (for production)
└── ...
```


## **Differences Between Lift 3 & 9**

What the frick-frack do all these classes, global variables & methods mean!? Well, to start off it's best to understand how the code-bases between the two lifts differ:
 - **Lift 3** has an `ENABLE_LEDS` flag (`#define ENABLE_LEDS`) to toggle whether LEDs are enabled; the flag can be set to either 1 to enable or 0 to disable.

 - **Lift 9** has an `ENABLE_DOORS` flag (`#define ENABLE_DOORS`) to toggle whether the open & close door buttons are enabled; the flag can be set to either 1 to enable or 0 to disable.

 - **Lift 9** has two additionally-declared `Button` variables (or objects), namedly `closeDoorsButton` on analog port `A0` and `openDoorsButton` on analog port `A1`.

 - **Lift 9** has two additional methods: the `openDoors()` method which will send the character `'o'` over comms to the state machine to open the lift doors and the `closeDoors()` method which will send the character `'c'` over comms to the state machine to close the lift doors.


## **Classes**

Both lift's have two classes to simplify the creation of new **buttons** & **LEDs**, they can be used as follows:
 
### **Using `Button` class**

**1)** Create a new instance of class `Button` with the name of your button and the pin the button is wired to: 
```
Button myButton(420); // Create a button called 'myButton' on pin 420
``` 

**2)** Set the `pinMode` by calling the `begin()` method on your newly created button in the `setup()` method: 
 ```
void setup() {
    myButton.begin(); // Set the pinMode to 420 (previously declared)
}
 ```

 **3)** Listen to button presses by checking the `isReleased()` method on your button in the `loop()` method:
 ```
 void loop() {
     if (myButton.isReleased()) {
         // Button has been pressed!
     }
 }
 ```

 ### **Using `LED` class**

**1)** Create a new instance of class `LED` with the name of your LED and the pin the LED is wired to: 
```
LED myLED(21); // Create an LED called 'myLED' on pin 21
``` 

**2)** Set the `pinMode` by calling the `begin()` method on your newly created LED in the `setup()` method: 
 ```
void setup() {
    myLED.begin(); // Set the pinMode to 21 (previously declared)
}
 ```

 **3)** You can turn your LED on & off using the respective `on()` & `off()` methods:
 ```
myLED.on(); // Turn myLED on
delay(2000); // Wait 2 seconds until we turn myLED off again
myLED.off(); // Turn myLED off
 ```


## **Global Variables**

There are three types of global variables used in both code-bases: *normal variables* (variables which **can** be changed by methods), *constants* (those starting with `const` which **can't** be changed by methods) and *objects* (in this case, variables of the type `Button` & `LED` classes). Additionally, **global** variables refer to the variables located outside of a method (not in `setup()`, `loop()` etc.) and are usually located at the very top of the code. To avoid repeating the comments scattered throughout the actual code, this section will explain the more complex variable declarations, including the variables: `floorButtons` & `floorButtonLEDs`.

### **Using `floorButtons` and `floorButtonLEDs`**

The arrays `floorButtons` & `floorButtonLEDs` allow us to easily iterate through all floor buttons & floor button LEDs and apply the same action, method(s) and checks without having to call each individual button or LED object individually.

The syntax may look daunting, but can be easily explained when disassembled, lets have a look at the different parts essential to both arrays `floorButtons` & `floorButtonLEDs`:

**1)** First off, the constant variable `totalFloors` contains the total number of floors in the lift and, therefore, also the total number of floor buttons and floor button LEDs in the control box of the lift. Whenever a new floor button is added or removed (items are added or removed from both arrays `floorButtons` & `floorButtonLEDs`), this variable should be changed to match the total number of buttons/LEDs in both arrays. This variable assumes that the total number of floor buttons and floor button LEDs will always be the same.
```
// Total number of floors, total number of floor buttons & total number of floor button LEDs

const int totalFloors = 5; // 5 floors in lift
```

**2)** The declaration of both arrays `floorButtons` & `floorButtonLEDs` are very similar. Declaration occurs at the very top of the code and will declare the pins for all floor buttons and floor button LEDs, remember that declaration does **not** set the `pinMode`, you'd have to call the `begin()` method on all items in both arrays in the `setup()` method. The order in which each item is declared is relative to the floor number of that item, so the first item in each array would always be for floor button 1 and floor button LED 1. This flexibility makes switching pins easy and efficient, as changing a pin for a floor button or floor button LED will not break any code.
```
// An array of Buttons for all floor buttons
Button floorButtons[totalFloors] = {
    Button(3), // Floor 1 button set to pin 3
    Button(4), // Floor 2 button set to pin 4
    Button(5), // Floor 3 button set to pin 5
    Button(6), // Floor 4 button set to pin 6
    Button(7)  // Floor 5 button set to pin 7
};

// An array of LEDs for all floor button LEDs
LED floorButtonLEDs[totalFloors] = {
    LED(9),  // Floor 1 button LED set to pin 9
    LED(10), // Floor 2 button LED set to pin 10
    LED(11), // Floor 3 button LED set to pin 11
    LED(12), // Floor 4 button LED set to pin 12
    LED(13)  // Floor 5 button LED set to pin 13
};
```

**3)** Accessing a single floor button or floor button LED is predominantly the same as before, with the exception of the array syntax (remember arrays start at 0; therefore, 0 is the first floor and so on):
```
floorButtonLEDs[0].on(); // Turn on the LED for floor button 1
```
In addition to accessing individual floor buttons and floor button LEDs, we also get the added benefits which come with arrays. This means we can iterate over all the items in either array and do the same thing to all items. For example, checking whether all floor buttons have been pressed and released in the array `floorButtons`:
```
// Will loop from floor button 0 to 5 (value of totalFloors)
for (int i = 0; i < totalFloors; i++) {
    if (floorButtons[i].isReleased()) {
        // Floor button at index i has been pressed!
    }
}
```
Or even turning-on all floor button LEDs:
```
// Will loop from floor button 0 to 5 (value of totalFloors)
for (int i = 0; i < totalFloors; i++) {
    floorButtonLEDs[i].on(); // Turn on floor button LED at index i
}
```
Essentially, if you want to do the same thing to numerous floor buttons or floor button LEDs, you'd most likely use a for loop as the ones above (albeit the ones above do the same thing to **ALL** items).


## **Methods**

Methods (or functions) are the bits of code which *do* things, it's always good practice to keep repetitive segments of code in a method. To avoid repeating the comments scattered throughout the actual code, this section will explain the more complex methods, including the methods: `goToFloor()` & `checkLiftResponse()`.

### **Using `goToFloor()`**

The method `goToFloor()` contains one parameter: `floorIndex` which accepts a variable of type `int`, this paramter corresponds to which floor you'd like the lift to go to. This method will first check if the requested floor in `floorIndex` is different from the current floor and that the `floorIndex` is in range 0 and `totalFloors`, once this check is complete the selected floor button LED will be turned on to indicate to the user that the floor has been selected. The stop button LED will also be turned off to indicate that the lift is moving. The `floorIndex`+1 will be sent over comms to the Arduino MEGA; since it's an index, 0 equates to floor 1 and so on (this index just makes it easier to work with the arrays `floorButtons` & `floorButtonLEDs` declared previously). The new current floor index will be stored in the `currentFloorIndex` variable so we don't try to go to the same floor twice.

The syntax should be fairly self-explanatory for this one, if you want to call the method you'd do something like this:
```
goToFloor(0); // Move the lift to floor 1
```

### **Using `checkLiftResponse()`**

The method `checkLiftResponse()` is destined for the `loop()` method, it continuously checks for a response from the Arduino MEGA and expects the following in response:

 - `0` = Don't do anything
 - `1` to `totalFloors` = Turn off the floor button LED at this index
 - `-1` = An error occurred, turn on the stop button LED

 Since we're sending ASCII values over comms, we'll need to convert the ASCII response from the Arduino MEGA into integer values. This is done by reading the bytes of the response from the Arduino MEGA into a buffer and then converting the received ASCII value into an integer by subtracting 48 from the ASCII value in decimal form. 