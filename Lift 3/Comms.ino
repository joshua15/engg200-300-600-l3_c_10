/*
  Comms - Arduino MEGA
  Team: L3_C_10
  Members: Elizabeth Laham, JiGar Jetwani, Nathan Lecompte, Joshua Archer & Chengen Cui
*/

/* NOTICE: 
	Lift 3 has already written all this code,
	it will listen for commands on baudrate 3600
	(0 = Do nothing (default); 1 = Stop lift; 2 = Go up; 3 = Go down)
*/

// Address for comms link
const long commsAddress = 9600;

// Max number of floors
const int numFloors = 5;

// Current command sent over from Control Box
// (0 = Do nothing (default); 1 = Stop lift; 2 = Go up; 3 = Go down)
int controlBoxCommand = 0;

// Current floor
int currentFloor = 1;


// Main setup function which initialises everything
void setup() {
	setupCommsLink();
}

// Function to setup comms link to RX Arduino UNO
void setupCommsLink() {
	Serial.begin(commsAddress);
}


// Main loop function which listens for changes
void loop() {
	if(receiveComms() != 0) {

		if(controlBoxCommand == 1) {

			// Stop lift
			// TODO: CALL STATE MACHINE API HERE

		} else if(controlBoxCommand == 2) {

			// Go up one floor
			if(currentFloor != numFloors) {
				currentFloor++;
			}

			// TODO: CALL STATE MACHINE API HERE

		} else if(controlBoxCommand == 3) {

			// Go down one floor
			if(currentFloor != 1) {
				currentFloor--;
			}

			// TODO: CALL STATE MACHINE API HERE
			
		}

		// Send success/failure response to Control Box (0 = Success; 1 = Error)
		sendComms(0);
		controlBoxCommand = 0;
	}
}

// Function to send a value to Control Box (Arduino UNO)
void sendComms(int value) {
	Serial.print(value);
}

// Function to receive a floor value from Control Box
// (0 = Do nothing (default); 1 = Stop lift; 2 = Go up; 3 = Go down)
int receiveComms() {
	while(Serial.available() != 0) {
        controlBoxCommand = Serial.parseInt();
    }
	return controlBoxCommand;
}