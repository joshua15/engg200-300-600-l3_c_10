/*
  Control Box - Arduino UNO (Lift 3)
  Team: L3_C_10
  Members: Elizabeth Laham, JiGar Jethwani, Nathan Lecompte, Joshua Archer & Chengen Cui
*/

#define DEBUG 0
#define ENABLE_LEDS 0
#define ENABLE_LCD 1

#if ENABLE_LCD
    #include <LiquidCrystal_I2C.h>
#endif

class Button {
    private:
        bool buttonState;
        uint8_t buttonPin;

    public:
        Button(uint8_t pin) : buttonPin(pin) {}

        // Set pinMode for Button
        void begin() {
            pinMode(buttonPin, INPUT);
            buttonState = 1;
        }

        // Function to check if Button is released
        bool isReleased() {
            bool value = digitalRead(buttonPin);
            if (value != buttonState) {
                buttonState = value;
                if (buttonState) return true;
            }
            return false;
        }
};

#if ENABLE_LEDS
    class LED {
        private:
            uint8_t LEDPin;

        public:
            LED(uint8_t pin) : LEDPin(pin) {}

            // Set pinMode for LED
            void begin() {
                pinMode(LEDPin, OUTPUT);
            }

            // Turn on the LED
            void on() {
                digitalWrite(LEDPin, HIGH);
            }

            // Turn off the LED
            void off() {
                digitalWrite(LEDPin, LOW);
            }
    };
#endif

const int totalFloors = 5;
const long commsAddress = 9600;

Button stopButton(7);
Button floorButtons[totalFloors] = {
    Button(2), // 1
    Button(3), // 2
    Button(4), // 3
    Button(5), // 4
    Button(6)  // 5
};

#if ENABLE_LEDS
    LED stopButtonLED(8);
    LED floorButtonLEDs[totalFloors] = {
        LED(9),  // 1
        LED(10), // 2
        LED(11), // 3
        LED(12), // 4
        LED(13)  // 5
    };
#endif

#if ENABLE_LCD
    LiquidCrystal_I2C LCD(0x27, 20, 4);
    int selectedFloors[totalFloors] = {
        0,0,0,0,0
    };
#endif


void setup() {

    // Set pinMode for all buttons
    stopButton.begin();
    for (int i = 0; i < totalFloors; i++) {
        floorButtons[i].begin();
    }

    #if ENABLE_LEDS
        // Set pinMode for all LEDs
        stopButtonLED.begin();
        for (int i = 0; i < totalFloors; i++) {
            floorButtonLEDs[i].begin();
        }
    #endif

    #if ENABLE_LCD
        // Setup LCD display
        LCD.init();
        LCD.backlight();
        LCD.setCursor(0, 0);
        LCD.print("Welcome");
        LCD.setCursor(0, 1);
    #endif

    // Setup comms link
    Serial.begin(commsAddress);

    #if DEBUG
        Serial.print("Connected!\n");
    #endif
}

void loop() {

    // Check if stop button is pressed
    if (stopButton.isReleased()) stopLift();
    
    #if ENABLE_LEDS
        // Check for a respone from the Arduino MEGA
        checkLiftResponse();
    #endif

    // Check if floor button is pressed
    for (int i = 0; i < totalFloors; i++) {
        Button btn = floorButtons[i];
        if (floorButtons[i].isReleased()) goToFloor(i);
    }
}

// Send 's' over comms to stop lift
void stopLift() {
    #if DEBUG
        Serial.print("Stopping lift... \n");
    #endif

    #if ENABLE_LCD
        // Set LCD message
        for (int i = 0; i < totalFloors; i++) {
            selectedFloors[i] = 0;
        }
        LCD.clear();
        LCD.setCursor(0, 0);
        LCD.print("Lift stopped");
    #endif

    #if ENABLE_LEDS
        for (int i = 0; i < totalFloors; i++) {
            floorButtonLEDs[i].off();
        }
        stopButtonLED.on();
    #endif

    // Send command over comms
    Serial.write("s");
}

#if ENABLE_LCD
    // Print selected floors to LCD display
    void printSelectedFloorsToLCD() {
        LCD.clear();
        LCD.setCursor(0, 0);
        LCD.print("Going to:");
        LCD.setCursor(0, 1);
        for (int i = 0; i < totalFloors; i++) {
            if (selectedFloors[i] != 0) {
                LCD.print((char)(selectedFloors[i] + 48));
                LCD.print(" ");
            }
        }
    }
#endif

// Send floor number over comms
void goToFloor(int floorIndex) {

    // Only go to floor if within range
    if (floorIndex >= 0 && floorIndex < totalFloors) {
        #if DEBUG
            Serial.print("Going to floor: ");
        #endif

        #if ENABLE_LEDS
            // Turn off stop LED
            stopButtonLED.off();

            // Turn on selected floor button LED
            floorButtonLEDs[floorIndex].on();
        #endif

        #if ENABLE_LCD
            // Set LCD message
            selectedFloors[floorIndex] = floorIndex + 1;
            printSelectedFloorsToLCD();
        #endif

        // Send floor number as ASCII over comms
        Serial.write((char)(floorIndex + 49));

        #if DEBUG
            Serial.print("\n");
        #endif
    }
}

// Listen for response from MEGA
void checkLiftResponse() {
    char buffer[10];
    buffer[0] = 0;

    if (Serial.available() != 0) {

        // Read response into buffer
        Serial.readBytes(buffer, 1);
        // Convert response from ASCII to int if buffer is not 0
        int response = (buffer[0] != 0) ? ((int)buffer[0]) - 48 : 0;

        if (response > 0 && response <= totalFloors) {
            #if ENABLE_LEDS
                // Turn off LED for floor reached success
                floorButtonLEDs[response - 1].off();
            #endif
            
            #if ENABLE_LCD
                // Set LCD message
                selectedFloors[response - 1] = 0;
                printSelectedFloorsToLCD();
            #endif

            #if DEBUG
              Serial.print("Arrived at floor: ");
              Serial.print(buffer[0]);
              Serial.print("\n");
            #endif
        } else {
            #if DEBUG
                Serial.print("Error: Unexpected response via comms\n");
            #endif
        }
    }
}
