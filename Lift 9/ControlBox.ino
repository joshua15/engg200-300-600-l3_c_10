/*
  Control Box - Arduino UNO (Lift 9)
  Team: L3_C_10
  Members: Elizabeth Laham, JiGar Jethwani, Nathan Lecompte, Joshua Archer & Chengen Cui
*/

#define DEBUG 0
#define ENABLE_STOP 1
#define ENABLE_LCD 1

#if ENABLE_LCD
    #include <LiquidCrystal_I2C.h>
#endif

class Button {
    private:
        bool buttonState;
        uint8_t buttonPin;

    public:
        Button(uint8_t pin) : buttonPin(pin) {}

        // Set pinMode for Button
        void begin() {
            pinMode(buttonPin, INPUT);
            buttonState = 0;
        }

        // Function to check if Button is released
        bool isReleased() {
            bool value = digitalRead(buttonPin);
            if (value != buttonState) {
                buttonState = value;
                if (buttonState) return true;
            }
            return false;
        }
};

class LED {
    private:
        uint8_t LEDPin;

    public:
        LED(uint8_t pin) : LEDPin(pin) {}

        // Set pinMode for LED
        void begin() {
            pinMode(LEDPin, OUTPUT);
        }

        // Turn on the LED
        void on() {
            digitalWrite(LEDPin, HIGH);
        }

        // Turn off the LED
        void off() {
            digitalWrite(LEDPin, LOW);
        }
};

const int totalFloors = 5;
const long commsAddress = 9600;

#if ENABLE_STOP
    Button stopButton(7);
#endif

Button floorButtons[totalFloors] = {
    Button(2), // 1
    Button(3), // 2
    Button(4), // 3
    Button(5), // 4
    Button(6)  // 5
};

#if ENABLE_STOP
    LED stopButtonLED(A3);
#endif

LED floorButtonLEDs[totalFloors] = {
    LED(8),  // 1
    LED(9), // 2
    LED(10), // 3
    LED(11), // 4
    LED(12)  // 5
};

#if ENABLE_LCD
    LiquidCrystal_I2C LCD(0x27, 20, 4);
    int selectedFloors[totalFloors] = {
        0, 0, 0, 0, 0
    };
#endif


void setup() {

    // Set pinMode for all buttons
    #if ENABLE_STOP
        stopButton.begin();
    #endif

    for (int i = 0; i < totalFloors; i++) {
        floorButtons[i].begin();
    }

    #if ENABLE_STOP
        // Set pinMode for all LEDs
        stopButtonLED.begin();
    #endif

    for (int i = 0; i < totalFloors; i++) {
        floorButtonLEDs[i].begin();
    }

    #if ENABLE_LCD
        // Setup LCD display
        LCD.init();
        LCD.backlight();
        LCD.setCursor(0, 0);
        LCD.print("Welcome");
        LCD.setCursor(0, 1);
    #endif

    // Setup comms link
    Serial.begin(commsAddress);

    #if DEBUG
        Serial.print("Connected!\n");
    #endif
}

void loop() {
    #if ENABLE_STOP
        // Check if stop button is pressed
        if (stopButton.isReleased()) stopLift();
    #endif

    // Check for a respone from the Arduino MEGA
    checkLiftResponse();

    // Check if floor button is pressed
    for (int i = 0; i < totalFloors; i++) {
        Button btn = floorButtons[i];
        if (floorButtons[i].isReleased()) goToFloor(i);
    }
}

#if ENABLE_STOP
    // Send 's' over comms to stop lift
    void stopLift() {
        #if DEBUG
            Serial.print("Stopping lift... \n");
        #endif

        // Set LCD message
        #if ENABLE_LCD
            // Set LCD message
            for (int i = 0; i < totalFloors; i++) {
                selectedFloors[i] = 0;
            }
            LCD.clear();
            LCD.setCursor(0, 0);
            LCD.print("Lift stopped");
        #endif

        for (int i = 0; i < totalFloors; i++) {
            floorButtonLEDs[i].off();
        }
        stopButtonLED.on();

        Serial.write("s");
    }
#endif

#if ENABLE_LCD
    // Print selected floors to LCD display
    void printSelectedFloorsToLCD() {
        LCD.clear();
        LCD.setCursor(0, 0);
        LCD.print("Going to:");
        LCD.setCursor(0, 1);
        for (int i = 0; i < totalFloors; i++) {
            if (selectedFloors[i] != 0) {
                LCD.print((char)(selectedFloors[i] + 48));
                LCD.print(" ");
            }
        }
    }
#endif

// Send floor number over comms
void goToFloor(int floorIndex) {

    // Only go to floor if within range
    if (floorIndex >= 0 && floorIndex < totalFloors) {
        #if DEBUG
            Serial.print("Going to floor: ");
        #endif

        #if ENABLE_LCD
            // Set LCD message
            selectedFloors[floorIndex] = floorIndex + 1;
            printSelectedFloorsToLCD();
        #endif

        #if ENABLE_STOP
            // Turn off stop LED
            stopButtonLED.off();
        #endif

        // Turn on selected floor button LED
        floorButtonLEDs[floorIndex].on();

        // Send floor number as ASCII
        Serial.write((char)(floorIndex + 49));

        #if DEBUG
            Serial.print("\n");
        #endif
    }
}

// Listen for response from MEGA
void checkLiftResponse() {
    char buffer[10];
    buffer[0] = 0;

    if (Serial.available() != 0) {

        // Read response into buffer
        Serial.readBytes(buffer, 1);
        // Convert response from ASCII to int if buffer is not 0
        int response = (buffer[0] != 0) ? ((int)buffer[0]) - 48 : 0;

        if (response > 0 && response <= totalFloors) {

            // Turn off LED for floor reached success
            floorButtonLEDs[response - 1].off();

            #if ENABLE_LCD
                // Set LCD message
                selectedFloors[response - 1] = 0;
                printSelectedFloorsToLCD();
            #endif

            #if DEBUG
              Serial.print("Arrived at floor: ");
              Serial.print(buffer[0]);
              Serial.print("\n");
            #endif
        } else {
            #if DEBUG
                Serial.print("Error: Unexpected response via comms\n");
            #endif
        }
    }
}
